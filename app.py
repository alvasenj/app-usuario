from usuario_dao import UsuarioDao
from logger_base import logger
from usuario import Usuario

def listar_usuarios():
    usuarios = UsuarioDao.seleccionar()
    for usuario in usuarios:
        logger.info(usuario)

if __name__ == '__main__':
    opcion = None
    while(opcion != '5'):
        print(
            f'Opciones: \n'
            f'1. Listar usuarios: \n'
            f'2. Agregar usuario\n'
            f'3. Modificar usuario\n'
            f'4. Eliminar usuario\n'
            f'5. Salir'
            )
        opcion = input(f'Escribe tu opcion (1-5): ')
        if opcion == '1':
            listar_usuarios()
        elif opcion == '2':
            nombre = input(f'Escribe username: ')
            pwd = input(f'Escribe el password: ')
            usuario = Usuario(username=nombre, password=pwd)
            usuarios_insertados = UsuarioDao.insertar(usuario)
            logger.info(f'Usuarios insertados: {usuarios_insertados}')
        elif opcion == '3':
            listar_usuarios()
            id_usuario = input(f'Escribe el id_usuario a modificar: ')
            nombre = input(f'Escribe el nuevo username: ')
            pwd = input(f'Escribe el nuevo password: ')
            usuario = Usuario(id_usuario, nombre, pwd)
            usuarios_modificados = UsuarioDao.actualizar(usuario)
            logger.info(f'Usuarios modificados: {usuarios_modificados} - Id usuario: {id_usuario}')
        elif opcion == '4':
            listar_usuarios()
            id_usuario = input(f'Escribe el id_usuario a eliminar: ')
            usuario = Usuario(id_usuario)
            usuarios_eliminados = UsuarioDao.eliminar(usuario)
            logger.info(f'Usuarios eliminados: {usuarios_eliminados} - Id Usuario: {id_usuario}')

        