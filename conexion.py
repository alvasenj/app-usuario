from psycopg2 import pool
import parametros
from logger_base import logger
import sys

class Conexion:
    __DATABASE = parametros.database
    __USERNAME = parametros.user
    __PASSWORD = parametros.password
    __DB_PORT = parametros.port
    __HOST = parametros.host
    __MIN_CON = 1
    __MAX_CON = 5
    __pool = None
    
    @classmethod
    def obtener_pool(cls):
        if cls.__pool is None:
            try:
                cls.__pool = pool.SimpleConnectionPool(cls.__MIN_CON,
                                                    cls.__MAX_CON,
                                                    user = cls.__USERNAME,
                                                    password = cls.__PASSWORD,
                                                    database = cls.__DATABASE,
                                                    port = cls.__DB_PORT,
                                                    host = cls.__HOST
                                                    )
                logger.debug('Creando pool de conexiones...{cls.__pool}')
            except Exception as e:
                logger.error(f'Error en la creacion del pool de conexiones {cls.__pool}')
                sys.exit()
            return cls.__pool
        else:
            return cls.__pool

    @classmethod
    def obtener_conexion(cls):
        conexion = cls.obtener_pool().getconn()
        logger.debug(f'Creando conexion...{conexion}')
        return conexion

    @classmethod
    def liberar_conexion(cls, conexion):
        cls.obtener_pool().putconn(conexion)
        logger.debug(f'Devolviendo conexion al pool: {cls.__pool}')
        logger.debug(f'Estado del pool: {cls.__pool}')
        
    @classmethod
    def cerrar_conexiones(cls):
        cls.obtener_pool().closeall()
        logger.debug(f'Cerrando conexiones del pool: {cls.__pool}')
    
if __name__ == '__main__':
    #Obtener una conexion a traves del pool
    conexion1 = Conexion.obtener_conexion()
    conexion2 = Conexion.obtener_conexion()
    #Devolvemos las conexiones
    Conexion.liberar_conexion(conexion1)
    Conexion.liberar_conexion(conexion2)
    #Cerramos el pool
    Conexion.cerrar_conexiones()
    #Si intentamos pedir una conexion de un pool cerrado
    #conexion3 = Conexion.obtener_conexion()