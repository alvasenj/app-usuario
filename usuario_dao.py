from cursor_pool import CursorPool
from usuario import Usuario
from logger_base import logger

class UsuarioDao:
    __SELECCIONAR = 'SELECT * FROM usuario ORDER BY id_usuario'
    __INSERTAR = 'INSERT INTO usuario(username, password) VALUES (%s, %s)'
    __ACTUALIZAR = 'UPDATE usuario SET username=%s, password=%s WHERE id_usuario=%s'
    __ELIMINAR = 'DELETE FROM usuario WHERE id_usuario=%s'
    
    @classmethod
    def seleccionar(cls):
        with CursorPool() as cursor:
            logger.debug(cursor.mogrify(cls.__SELECCIONAR))
            cursor.execute(cls.__SELECCIONAR)
            registros = cursor.fetchall()
            usuarios = []
            for registro in registros:
                usuario = Usuario(registro[0], registro[1], registro[2])
                usuarios.append(usuario)
            return usuarios
        
    @classmethod
    def insertar(cls, usuario):
        with CursorPool() as cursor:
            logger.debug(cursor.mogrify(cls.__INSERTAR))
            valores = (usuario.get_username(), usuario.get_password())
            cursor.execute(cls.__INSERTAR, valores)
            return cursor.rowcount
    
    @classmethod
    def actualizar(cls, usuario):
        with CursorPool() as cursor:
            logger.debug(cursor.mogrify(cls.__ACTUALIZAR))
            valores = (usuario.get_username(), usuario.get_password(), usuario.get_id_usuario())
            cursor.execute(cls.__ACTUALIZAR, valores)
            return cursor.rowcount
    
    @classmethod
    def eliminar(cls, usuario):
        with CursorPool() as cursor:
            logger.debug(cursor.mogrify(cls.__ELIMINAR))
            valores = (usuario.get_id_usuario(),)
            cursor.execute(cls.__ELIMINAR, valores)
            return cursor.rowcount
        

if __name__ == '__main__':
    # usuarios = UsuarioDao.seleccionar()
    # for usuario in usuarios:
    #     logger.debug(usuario) 
    usuario = Usuario(username='cola', password='pilila')
    usuarios_insertados = UsuarioDao.insertar(usuario)
    logger.debug(f'Usuarios insertados: {usuarios_insertados}')
    # usuario = Usuario(id_usuario=1, username='cola', password='pene')
    # usuarios_actualizados = UsuarioDao.actualizar(usuario)
    # logger.debug(f'Usuarios actualizados: {usuarios_actualizados}')
    # usuario = Usuario(1)
    # usuarios_eliminados = UsuarioDao.eliminar(usuario)
    # logger.debug(f'Usuarios eliminados: {usuarios_eliminados}')